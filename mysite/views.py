from django.shortcuts import render
from shop.models import Item

def shop_main(request):
    inventory = Item.objects.all()
    return render(request, 'shop/shop_main.html', {'inventory': inventory})

def shop_buy(request):
    ItemShop = request.GET.get('itemID')
    amount = request.GET.get('amount')
    buyVictim = Item.objects.get(itemID=ItemShop)
    stock = buyVictim.itemStock
    # =========================
    if int(amount)>int(stock):
        inventory = Item.objects.all()
        return render(request, 'shop/shop_main.html', {'inventory': inventory})
    # =========================
    buyVictim.itemStock = stock-int(amount)
    buyVictim.save()
    price = buyVictim.itemPrice
    # =========================
    finalPrice = float(price)*float(amount)
    # =========================
    return render(request, 'shop/purchase.html', {'buyVictim': buyVictim,'finalPrice': finalPrice})
