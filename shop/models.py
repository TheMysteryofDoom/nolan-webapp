# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Item(models.Model):
	itemID = models.IntegerField()
	itemName = models.TextField()
	itemPrice = models.FloatField()
	itemPic = models.ImageField()
	itemDescription = models.TextField()
	itemStock = models.IntegerField()

	def getItemName(self):
		return self.itemName
